import { Router } from 'express';
import { userController } from '../controller/user-controller';

export const userRoutes = (app: any) => {
    const router = Router();
    
    router.get('/users', userController.getAllUsersController);
    router.get('/users/:id', userController.userByIdController);
    router.post('/users', userController.newUserController);
    router.put('/users/:id', userController.updateUserByIdController);
    router.delete('/users/:id', userController.deleteUserByIdController);

    app.use(router);
}
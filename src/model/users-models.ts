import { Schema, model } from 'mongoose';

// Definir el esquema y el modelo de Mongoose

const userSchema = new Schema({
    name: String,
    age: Number
},{
    versionKey: false
});
export const userModel = model('Usuario', userSchema);
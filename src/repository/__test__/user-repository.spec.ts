import { userModel } from '../../model/users-models';
import { repositoryUser } from '../user-repository';


describe('Prueba sobre el crud', () =>{
    it('userById',async () => {
        jest.spyOn(userModel, 'findById').mockResolvedValue([])
        const retriveUserById = await repositoryUser.userById('')
        expect(retriveUserById).toEqual([])
    })

    it('user by id ERROR',async () => {
        const mockId = "64c188eb2610600ba2828efd"
        const mockError = new Error('Error al obtener al usuario por su ID.');
        jest.spyOn(userModel, 'findById').mockRejectedValue(mockError);
        try {
            await repositoryUser.userById(mockId);
        } catch (error: any) {
            expect(error.message).toBe('Error al obtener al usuario por su ID.');
        }
    });

    it('get all',async () => {
        const mockResult=[
            {
                "_id": "64c188eb2610600ba2828efd",
                "name": "Arthur",
                "age": 36
            }
        ]
        jest.spyOn(userModel,"find").mockResolvedValue(mockResult)
        const res = await repositoryUser.getAllUsers()
        expect(res).toEqual(mockResult);
    });

    it('get all ERROR', async () => {
        const mockError = new Error('Error al obtener todos los usuarios de la base de datos.');
        jest.spyOn(userModel, "find").mockRejectedValue(mockError);
        try {
            await repositoryUser.getAllUsers();
        } catch (error: any) {
            expect(error.message).toBe('Error al obtener todos los usuarios de la base de datos.');
        }
    });


    it('Update User by Id', async () => {
        const id = "64c188eb2610600ba2828efd"
        const mockNewData = {
            "name": "Arturo",
            "age": 36
        }
        const mockResult = {
            "_id": "64c188eb2610600ba2828efd",
            "name": "Arturo",
            "age": 36
        }
        jest.spyOn(userModel,"findByIdAndUpdate").mockResolvedValue(mockResult);
        const res = await repositoryUser.updateUserById(id, mockNewData);
        expect(res).toEqual(mockResult);
    });

    it('Update user by id ERROR',async () => {
        const mockId = "64c188eb2610600ba2828efd"
        const mockNewData = {
            "name": "Arturo",
            "age": 36
        }
        const mockError = new Error('Error al actualizar a un ususario.');
        jest.spyOn(userModel, 'findByIdAndUpdate').mockRejectedValue(mockError);
        try {
            await repositoryUser.updateUserById(mockId, mockNewData);
        } catch (error: any) {
            expect(error.message).toBe('Error al actualizar a un ususario.');
        }
    });

    it('Delete by id',async () => {
        const id = "64c188eb2610600ba2828efd"
        const mockResult = {
            "_id": "64c188eb2610600ba2828efd",
            "name": "Arthur",
            "age": 36
        }
        jest.spyOn(userModel, "findByIdAndDelete").mockResolvedValue(mockResult);
        const res = await repositoryUser.deleteUserById(id);
        expect(res).toEqual(mockResult);
    });

    it('Delete user by id ERROR',async () => {
        const mockId = "64c188eb2610600ba2828efd"
        const mockError = new Error('Error al eliminar a un ususario.');
        jest.spyOn(userModel, 'findByIdAndDelete').mockRejectedValue(mockError);
        try {
            await repositoryUser.deleteUserById(mockId);
        } catch (error: any) {
            expect(error.message).toBe('Error al eliminar a un ususario.');
        }
    });

    /* it('New user', async () => {
        
        const mockData = {
            "name": "Arthur",
            "age": 36
        };

        const mockUserSaved = {
            "_id": "64c188eb2610600ba2828efd",
            "name": "Arthur",
            "age": 36
        };

        jest.spyOn(userModel, "create").mockResolvedValue(mockUserSaved);
        const res = await repositoryUser.newUser(mockData);
        expect(res).toEqual(mockData);
    }); */
});

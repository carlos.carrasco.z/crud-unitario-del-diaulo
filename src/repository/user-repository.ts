import { userModel } from '../model/users-models';

// Obtener todos los usuarios
const getAllUsers = async () => {
    try {
        const users = await userModel.find();
        return users;
      } catch (error: any) {
        throw new Error('Error al obtener todos los usuarios de la base de datos.');
      }
}

// Obtener un usuario por su ID
const userById = async (id: any) => {
    try{
        const user = await userModel.findById(id);
        return user;
    } catch(error){
        throw new Error('Error al obtener al usuario por su ID.');
    }
}

// Crear un nuevo usuario
const newUser = async (data: any) => {
    try {
        const userSaved = await userModel.create(data);
        return userSaved;
      } catch (error: any) {
        throw new Error('Error al crear a un ususario.');
      }
}
  
// Actualizar un usuario por su ID
const updateUserById = async (id: any, data: any) => {
    try {
        const userUpdated = await userModel.findByIdAndUpdate(id, data, { new: true });
        return userUpdated;
    } catch (error: any) {
        throw new Error('Error al actualizar a un ususario.');
    }
}
  
// Eliminar un usuario por su ID
const deleteUserById = async (id: any) => {
    try {
        const userDeleted = await userModel.findByIdAndDelete(id);
        return userDeleted;
    } catch (error: any) {
        throw new Error('Error al eliminar a un ususario.');
    }
}

export const repositoryUser = {
    getAllUsers,
    userById,
    newUser,
    updateUserById,
    deleteUserById
}
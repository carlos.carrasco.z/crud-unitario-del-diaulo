const userMock = {
    "_id": "64bee443b681f812e9ba4d90", 
    "name": "Arthur", 
    "age": 36
};

const getUserMock = jest.fn().mockRejectedValueOnce([userMock]);
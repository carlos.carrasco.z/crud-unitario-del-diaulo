import { repositoryUser } from "../repository/user-repository";

// Obtener todos los usuarios
const getAllUsersController = async(req: any, res: any) => {
    try {
        const users = await repositoryUser.getAllUsers();
        res.json(users);
      } catch (error: any) {
        res.status(500).json({ error: error.message });
      }
}

const userByIdController = async (req: any, res: any) => {
    const { id } = req.params;
    try {
        const user = await repositoryUser.userById(id);
        if (!user) {
          return res.status(404).json({error: 'Usuario no encontrado'});
        }
        res.json(user);
      } catch (error: any) {
        res.status(500).json({ error: error.message });
      }
}

const newUserController = async (req: any, res: any) => {
    const data = req.body
    try {
        const newUser = await repositoryUser.newUser(data);
        res.status(201).json(newUser);
      } catch (error: any) {
        res.status(500).json({ error: error.message });
      }
}

const updateUserByIdController = async (req: any, res: any) => {
    const { id } = req.params;
    const data = req.body;
    try {
        const userUpdated = await repositoryUser.updateUserById(id, data);
        if(!userUpdated){
            res.status(404).json({ message: 'Usuario no encontrado' });
        }
        res.json(userUpdated);
    } catch (error: any) {
        res.status(500).json({ error: error.message });
    }
}

const deleteUserByIdController = async (req: any, res: any) => {
    const { id } = req.params;
    try {
        const user = await repositoryUser.deleteUserById(id);
        if (!user) {
            res.status(404).json({ message: 'Usuario no encontrado' });
        }
        res.json({ message: 'Usuario eliminado correctamente' });
    } catch (error: any) {
        res.status(500).json({ error: error.message });
    }
}

export const userController = {
    getAllUsersController,
    userByIdController,
    newUserController,
    updateUserByIdController,
    deleteUserByIdController
}; 
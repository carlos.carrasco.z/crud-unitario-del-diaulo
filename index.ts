import express from 'express';
import bodyParser from 'body-parser';
import { userRoutes } from './src/router/userRouter';
import { conexionBase } from './src/config/configDatabase';

const app = express();
app.use(bodyParser.json());

userRoutes(app);

// Iniciar el servidor
const PORT = 3000;
app.listen(PORT, async () => {
  await conexionBase();
  console.log(`Servidor iniciado en el puerto ${PORT}`);
});